package krd.kurdistan.bootcamp.views;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import krd.kurdistan.bootcamp.user_app.UserDTO;
import krd.kurdistan.bootcamp.user_app.UserProxy;

import java.util.List;

@Route(value = "UserView", layout = MainLayout.class)
@PageTitle("User | Vaadin ")
public class UserView extends VerticalLayout {

    Grid<UserDTO> grid;

    UserProxy userProxy;

    TextField firstNameText;
    TextField lastNameText;
    TextField emailText;
    TextField bioText;
    TextField userNameText;
    TextField passwordText;
    TextField phoneNumberText;




    public UserView(UserProxy userProxy) {
        this.userProxy = userProxy;
        firstNameText = new TextField("FirstName");
        lastNameText=new TextField("LastName");
        emailText=new TextField("Email");
        bioText=new TextField("Bio");
        userNameText=new TextField("UserName");
        passwordText=new TextField("Password");
        phoneNumberText=new TextField("PhoneNumber");



        Button createButton = new Button("Save");
        createButton.addThemeVariants(ButtonVariant.LUMO_LARGE);

        createButton.addClickListener(buttonClickEvent -> {
            UserDTO userDTO =new UserDTO();
            userDTO.setFirstName(firstNameText.getValue());
            userDTO.setLastName(lastNameText.getValue());
            userDTO.setBio(bioText.getValue());
            userDTO.setEmail(emailText.getValue());
            userDTO.setPhone(phoneNumberText.getValue());
            userDTO.setUserName(userNameText.getValue());
            userDTO.setPassword(passwordText.getValue());
            userDTO.setPhone(phoneNumberText.getValue());
            userProxy.save(userDTO);
            System.out.println("success done");
            Notification notification = Notification.show("Successfully submitted!");
            notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
            fillGrid();

        });




        VerticalLayout verticalLayout=new VerticalLayout(firstNameText,lastNameText,userNameText,passwordText
                ,emailText,phoneNumberText,bioText,createButton);
        add(verticalLayout);



        /////////////////make grid


        grid = new Grid<>(UserDTO.class, false);
        grid.addColumn(UserDTO::getId).setHeader("Id");
        grid.addColumn(UserDTO::getFirstName).setHeader("FirstName");
        grid.addColumn(UserDTO::getLastName).setHeader("LastName");
        grid.addColumn(UserDTO::getBio).setHeader("Bio");
        grid.addColumn(UserDTO::getEmail).setHeader("Email");
        grid.addColumn(UserDTO::getUserName).setHeader("UserName");
        grid.addColumn(UserDTO::getPassword).setHeader("Password");
        grid.addColumn(UserDTO::getPhone).setHeader("PhoneNumber");




        grid.addSelectionListener(selectionEvent -> {
            selectionEvent.getFirstSelectedItem().ifPresent(carCategory -> {
                Notification.show(carCategory.getId() + " is selected");
            });
        });

        fillGrid();


        add(grid);

    }

    public  void fillGrid(){

        List<UserDTO> userDTOList= userProxy.getAll();

        grid.setItems(userDTOList);
    }
    public UserDTO findById(Long id){
       return userProxy.findById(id);
    }


    }
