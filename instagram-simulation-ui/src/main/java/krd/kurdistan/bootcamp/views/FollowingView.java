package krd.kurdistan.bootcamp.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import krd.kurdistan.bootcamp.follower.FollowerDTO;
import krd.kurdistan.bootcamp.following.FollowingDTO;
import krd.kurdistan.bootcamp.following.FollowingProxy;

import java.util.List;

@Route(value = "FollowingView", layout = MainLayout.class)
@PageTitle("Following | Vaadin ")
public class FollowingView extends VerticalLayout {

    Grid<FollowingDTO> grid;

    FollowingProxy followingProxy;

    TextField userNameText;

    TextField profileImageText;

    TextField removeFromFollowingText;

    TextField userIdText;



    public FollowingView(FollowingProxy followingProxy) {
        this.followingProxy = followingProxy;
        userNameText = new TextField("UserName");
        profileImageText=new TextField("Profile");
        removeFromFollowingText=new TextField("Remove");
        userIdText=new TextField("User");



        Button createButton = new Button("Save");
        createButton.addThemeVariants(ButtonVariant.LUMO_LARGE);

        createButton.addClickListener(buttonClickEvent -> {
            FollowingDTO followingDTO =new FollowingDTO();
            followingDTO.setUserName(userNameText.getValue());
            followingDTO.setProfileImage(profileImageText.getValue());
            followingDTO.setRemoveFromFollowing(Boolean.parseBoolean(removeFromFollowingText.getValue()));
            followingDTO.setUserId(Long.parseLong(userIdText.getValue()));
            followingProxy.save(followingDTO);
            System.out.println("success done");
            Notification notification = Notification.show("Successfully submitted!");
            notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
            fillGrid();

        });



        VerticalLayout verticalLayout=new VerticalLayout(userNameText,profileImageText,removeFromFollowingText,userIdText,createButton);
        add(verticalLayout);



        /////////////////make grid


        grid = new Grid<>(FollowingDTO.class, false);
        grid.addColumn(FollowingDTO::getId).setHeader("Id");
        grid.addColumn(FollowingDTO::getUserName).setHeader("UserName");
        grid.addColumn(FollowingDTO::getProfileImage).setHeader("Profile");
        grid.addColumn(FollowingDTO::getRemoveFromFollowing).setHeader("Following");
        grid.addColumn(FollowingDTO::getUserId).setHeader("User");





        grid.addSelectionListener(selectionEvent -> {
            selectionEvent.getFirstSelectedItem().ifPresent(carCategory -> {
                Notification.show(carCategory.getId() + " is selected");
            });
        });

        fillGrid();


        add(grid);
    }

    public  void fillGrid(){

        List<FollowingDTO> followingDTOS= followingProxy.getAll();

        grid.setItems(followingDTOS);
    }
    }
