package krd.kurdistan.bootcamp.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import krd.kurdistan.bootcamp.post.PostDTO;
import krd.kurdistan.bootcamp.post.PostProxy;
import krd.kurdistan.bootcamp.user_app.UserDTO;
import krd.kurdistan.bootcamp.user_app.UserProxy;

import java.util.List;

@Route(value = "PostView", layout = MainLayout.class)
@PageTitle("Post | Vaadin ")
public class PostView extends VerticalLayout {

    Grid<PostDTO> grid;

    PostProxy postProxy;

    TextField titleText;

    TextField postLinkText;

    TextField descriptionText;



    public PostView(PostProxy postProxy) {
        this.postProxy = postProxy;
        titleText = new TextField("Title");
        postLinkText=new TextField("Post");
        descriptionText=new TextField("Description");



        Button createButton = new Button("Save");
        createButton.addThemeVariants(ButtonVariant.LUMO_LARGE);

        createButton.addClickListener(buttonClickEvent -> {
            PostDTO postDTO =new PostDTO();
            postDTO.setTitle(titleText.getValue());
            postDTO.setPostLink(postLinkText.getValue());
            postDTO.setDescription(descriptionText.getValue());
            postProxy.save(postDTO);
            System.out.println("success done");
            Notification notification = Notification.show("Successfully submitted!");
            notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
            fillGrid();

        });



        VerticalLayout verticalLayout=new VerticalLayout(titleText,postLinkText,descriptionText,createButton);
        add(verticalLayout);



        /////////////////make grid


        grid = new Grid<>(PostDTO.class, false);
        grid.addColumn(PostDTO::getId).setHeader("Id");
        grid.addColumn(PostDTO::getTitle).setHeader("Title");
        grid.addColumn(PostDTO::getPostLink).setHeader("Post");
        grid.addColumn(PostDTO::getDescription).setHeader("Description");





        grid.addSelectionListener(selectionEvent -> {
            selectionEvent.getFirstSelectedItem().ifPresent(carCategory -> {
                Notification.show(carCategory.getId() + " is selected");
            });
        });

        fillGrid();


        add(grid);
    }

    public  void fillGrid(){

        List<PostDTO> userDTOList= postProxy.getAll();

        grid.setItems(userDTOList);
    }
    }
