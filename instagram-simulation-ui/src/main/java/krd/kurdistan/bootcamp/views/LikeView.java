package krd.kurdistan.bootcamp.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import krd.kurdistan.bootcamp.like.LikeDTO;
import krd.kurdistan.bootcamp.like.LikeProxy;
import krd.kurdistan.bootcamp.user_app.UserDTO;

import java.util.List;

@Route(value = "LikeView", layout = MainLayout.class)
@PageTitle("Like | Vaadin ")
public class LikeView extends VerticalLayout {

    Grid<LikeDTO> grid;

    LikeProxy likeProxy;

    TextField favoriteText;

    TextField postIdText;

    TextField userIdText;

    public LikeView(LikeProxy likeProxy) {
        this.likeProxy = likeProxy;
        favoriteText = new TextField("Favorite");
        postIdText=new TextField("Post");
        userIdText=new TextField("User");



        Button createButton = new Button("Save");
        createButton.addThemeVariants(ButtonVariant.LUMO_LARGE);

        createButton.addClickListener(buttonClickEvent -> {
            LikeDTO likeDTO =new LikeDTO();
            likeDTO.setFavorite(Boolean.valueOf(favoriteText.getValue()));
            likeDTO.setPostId(Long.parseLong(postIdText.getValue()));
            likeDTO.setUserId(Long.parseLong(userIdText.getValue()));
            likeProxy.save(likeDTO);
            System.out.println("success done");
            Notification notification = Notification.show("Successfully submitted!");
            notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
            fillGrid();

        });



        VerticalLayout verticalLayout=new VerticalLayout(favoriteText,postIdText,userIdText,createButton);
        add(verticalLayout);



        /////////////////make grid


        grid = new Grid<>(LikeDTO.class, false);
        grid.addColumn(LikeDTO::getId).setHeader("Id");
        grid.addColumn(LikeDTO::getFavorite).setHeader("Like");
        grid.addColumn(LikeDTO::getPostId).setHeader("Post");
        grid.addColumn(LikeDTO::getUserId).setHeader("User");





        grid.addSelectionListener(selectionEvent -> {
            selectionEvent.getFirstSelectedItem().ifPresent(carCategory -> {
                Notification.show(carCategory.getId() + " is selected");
            });
        });

        fillGrid();


        add(grid);
    }


    public  void fillGrid(){
        List<LikeDTO> likeDTOS= likeProxy.getAll();
        grid.setItems(likeDTOS);
    }
    }
