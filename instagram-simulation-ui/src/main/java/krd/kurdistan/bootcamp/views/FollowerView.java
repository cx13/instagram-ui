package krd.kurdistan.bootcamp.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import krd.kurdistan.bootcamp.comment.CommentDTO;
import krd.kurdistan.bootcamp.comment.CommentProxy;
import krd.kurdistan.bootcamp.follower.FollowerDTO;
import krd.kurdistan.bootcamp.follower.FollowerProxy;
import krd.kurdistan.bootcamp.user_app.UserDTO;
import krd.kurdistan.bootcamp.user_app.UserProxy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Route(value = "FollowerView", layout = MainLayout.class)
@PageTitle("Follower | Vaadin ")
public class FollowerView extends VerticalLayout {

    Grid<FollowerDTO> grid;

    FollowerProxy followerProxy;

    TextField userNameText;

    TextField profileImageText;

    TextField isFavoriteText;

    TextField userIdText;



   private UserDTO userDTO;
    @Autowired
    UserProxy userProxy;



    public FollowerView(FollowerProxy followerProxy) {
        this.followerProxy = followerProxy;
        userNameText = new TextField("UserName");
        profileImageText=new TextField("Profile");
        isFavoriteText=new TextField("Following");
        userIdText=new TextField("User");



        Button createButton = new Button("Save");
        createButton.addThemeVariants(ButtonVariant.LUMO_LARGE);

        createButton.addClickListener(buttonClickEvent -> {
            FollowerDTO followerDTO =new FollowerDTO();
            followerDTO.setUserName(userNameText.getValue());
            followerDTO.setProfileImage(profileImageText.getValue());
            followerDTO.setFollower(Boolean.parseBoolean(isFavoriteText.getValue()));
            Long id=Long.parseLong(userIdText.getValue());
            followerDTO.setUserId(id);
            followerProxy.save(followerDTO);
            System.out.println("success done");
            Notification notification = Notification.show("Successfully submitted!");
            notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
            fillGrid();

        });



        VerticalLayout verticalLayout=new VerticalLayout(userNameText,profileImageText,isFavoriteText,userIdText,createButton);
        add(verticalLayout);



        /////////////////make grid


        grid = new Grid<>(FollowerDTO.class, false);
        grid.addColumn(FollowerDTO::getId).setHeader("Id");
        grid.addColumn(FollowerDTO::getUserName).setHeader("UserName");
        grid.addColumn(FollowerDTO::getProfileImage).setHeader("Profile");
        grid.addColumn(FollowerDTO::getFollower).setHeader("Following");
        grid.addColumn(FollowerDTO::getUserId).setHeader("User");





        grid.addSelectionListener(selectionEvent -> {
            selectionEvent.getFirstSelectedItem().ifPresent(carCategory -> {
                Notification.show(carCategory.getId() + " is selected");
            });
        });

        fillGrid();


        add(grid);
    }

    public  void fillGrid(){

        List<FollowerDTO> followerDTOS= followerProxy.getAll();

        grid.setItems(followerDTOS);
    }
    }
