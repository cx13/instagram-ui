package krd.kurdistan.bootcamp.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import krd.kurdistan.bootcamp.comment.CommentDTO;
import krd.kurdistan.bootcamp.comment.CommentProxy;
import krd.kurdistan.bootcamp.like.LikeDTO;
import krd.kurdistan.bootcamp.like.LikeProxy;

import java.util.List;

@Route(value = "CommentView", layout = MainLayout.class)
@PageTitle("Comment | Vaadin ")
public class CommentView extends VerticalLayout {

    Grid<CommentDTO> grid;

    CommentProxy commentProxy;

    TextField messageText;

    TextField postIdText;

    TextField userIdText;

    public CommentView(CommentProxy commentProxy) {
        this.commentProxy = commentProxy;
        messageText = new TextField("Comment");
        postIdText=new TextField("Post");
        userIdText=new TextField("User");



        Button createButton = new Button("Save");
        createButton.addThemeVariants(ButtonVariant.LUMO_LARGE);

        createButton.addClickListener(buttonClickEvent -> {
            CommentDTO commentDTO =new CommentDTO();
            commentDTO.setMessage(messageText.getValue());
            commentDTO.setPostId(Long.parseLong(postIdText.getValue()));
            commentDTO.setUserId(Long.parseLong(userIdText.getValue()));
            commentProxy.save(commentDTO);
            System.out.println("success done");
            Notification notification = Notification.show("Successfully submitted!");
            notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
            fillGrid();

        });



        VerticalLayout verticalLayout=new VerticalLayout(messageText,postIdText,userIdText,createButton);
        add(verticalLayout);



        /////////////////make grid


        grid = new Grid<>(CommentDTO.class, false);
        grid.addColumn(CommentDTO::getId).setHeader("Id");
        grid.addColumn(CommentDTO::getMessage).setHeader("Comment");
        grid.addColumn(CommentDTO::getPostId).setHeader("Post");
        grid.addColumn(CommentDTO::getUserId).setHeader("User");





        grid.addSelectionListener(selectionEvent -> {
            selectionEvent.getFirstSelectedItem().ifPresent(carCategory -> {
                Notification.show(carCategory.getId() + " is selected");
            });
        });

        fillGrid();


        add(grid);
    }

        public  void fillGrid(){

        List<CommentDTO> commentDTOS= commentProxy.getAll();

        grid.setItems(commentDTOS);
    }
    }
