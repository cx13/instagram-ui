package krd.kurdistan.bootcamp.following;

import krd.kurdistan.bootcamp.like.LikeDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "following",url = "http://localhost:9595/v1/api-followingController")
public interface FollowingProxy {

    @GetMapping()
    List<FollowingDTO> getAll();

    @PostMapping("/v2")
    void save(@RequestBody FollowingDTO followingDTO);
}
