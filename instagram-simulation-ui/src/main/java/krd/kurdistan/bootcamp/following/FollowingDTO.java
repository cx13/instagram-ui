package krd.kurdistan.bootcamp.following;

public class FollowingDTO {

    private Long id;


    private String userName;


    private String profileImage;


    private Boolean removeFromFollowing;


    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Boolean getRemoveFromFollowing() {
        return removeFromFollowing;
    }

    public void setRemoveFromFollowing(Boolean removeFromFollowing) {
        this.removeFromFollowing = removeFromFollowing;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
