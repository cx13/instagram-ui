package krd.kurdistan.bootcamp.user_app;

import javax.validation.constraints.NotNull;

public class UserDTO {




    private Long id;

    private  String firstName;

    private  String lastName;


    private  String email;


    private  String phone;


    private  String password;


    private  String userName;


    private  String bio;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

//    public String getProfileImage() {
//        return profileImage;
//    }
//
//    public void setProfileImage(String profileImage) {
//        this.profileImage = profileImage;
//    }
//
//    public Long getPostCount() {
//        return postCount;
//    }
//
//    public void setPostCount(Long postCount) {
//        this.postCount = postCount;
//    }
//
//    public Long getFollowerCount() {
//        return followerCount;
//    }
//
//    public void setFollowerCount(Long followerCount) {
//        this.followerCount = followerCount;
//    }
//
//    public Long getFollowingCount() {
//        return followingCount;
//    }
//
//    public void setFollowingCount(Long followingCount) {
//        this.followingCount = followingCount;
//    }




//    private  String profileImage;
//
//
//    private Long postCount;
//
//
//    private Long followerCount;
//
//
//    private Long followingCount;
}
