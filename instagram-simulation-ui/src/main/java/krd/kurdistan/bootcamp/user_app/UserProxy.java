package krd.kurdistan.bootcamp.user_app;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "host",url = "http://localhost:9595/v1/api-userController")
public interface UserProxy {

    @GetMapping()
    List<UserDTO> getAll();

    @PostMapping
    void save(@RequestBody UserDTO userDTO);

    @GetMapping("{id}")
    UserDTO findById(@PathVariable Long id);
}
