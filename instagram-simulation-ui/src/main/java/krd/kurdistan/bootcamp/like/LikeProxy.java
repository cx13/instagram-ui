package krd.kurdistan.bootcamp.like;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "like",url = "http://localhost:9595/v1/api-likeController")
public interface LikeProxy {

    @GetMapping
    List<LikeDTO> getAll();

    @PostMapping("/v2")
    void save(@RequestBody LikeDTO likeDTO);
}
