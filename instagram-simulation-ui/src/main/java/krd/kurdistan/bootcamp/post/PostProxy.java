package krd.kurdistan.bootcamp.post;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "post",url = "http://localhost:9595/post")
public interface PostProxy {

    @GetMapping
    List<PostDTO> getAll();

    @PostMapping("/v1")
    void save(@RequestBody PostDTO postDTO);
}
