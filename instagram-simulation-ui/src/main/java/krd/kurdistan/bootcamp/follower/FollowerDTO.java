package krd.kurdistan.bootcamp.follower;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.VaadinSessionScope;
import krd.kurdistan.bootcamp.user_app.UserDTO;

import java.io.Serializable;

@SpringComponent
@VaadinSessionScope
public class FollowerDTO implements Serializable {

    private Long id;


    private String userName;


    private String profileImage;


    private Boolean isFollower;



    private Long userId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Boolean getFollower() {
        return isFollower;
    }

    public void setFollower(Boolean follower) {
        isFollower = follower;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
