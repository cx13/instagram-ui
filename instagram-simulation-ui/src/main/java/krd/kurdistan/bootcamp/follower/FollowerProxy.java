package krd.kurdistan.bootcamp.follower;

import krd.kurdistan.bootcamp.following.FollowingDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "follower",url = "http://localhost:9595/v1/api-followerController")
public interface FollowerProxy {

    @GetMapping
    List<FollowerDTO> getAll();

    @PostMapping("/v2")
    void save(@RequestBody FollowerDTO followerDTO);


}
