package krd.kurdistan.bootcamp.comment;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "comment",url = "http://localhost:9595/v1/api-commentController")
public interface CommentProxy {

    @GetMapping
    List<CommentDTO> getAll();

    @PostMapping("/v2")
    void save(@RequestBody CommentDTO commentDTO);
}
